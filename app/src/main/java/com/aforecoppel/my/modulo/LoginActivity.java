package com.aforecoppel.my.modulo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import static com.aforecoppel.my.modulo.Utilerias.*;

import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements AsyncTaskCompleteListener<String>,
        View.OnClickListener {

    // OBJETO PARA MANEJO DE SESIÓN
    private SesionModuloManager session;

    private String DIR_ACTUAL, DIR_ACTUALAFIL;

    public String ipAddress;
    public String ipAddressWsH;
    private final String apikey = BuildConfig.ApiKeyModul;

    // COMPONENTES PARA INICIO DE SESIÓN
    private EditText txtEmpleado;
    private Button btnLogin;

    // COMPONENTES PARA MOSTRAR ERROR
    private TextInputLayout lytNum;

    //LAYOUT PROGRESS BAR
    private ConstraintLayout pbCargando;

    //TextView para la version
    private TextView tvVersion;

    private boolean fEmp = false;

    //VARIABLES NECESARIAS
    private RequestQueue queue;
    private Spinner spinnerL;
    private int iEmpleado = 0;
    private int identificadorParam;
    private String identificador;
    private String todayDate;
    private String template;
    private boolean flag = false;
    private int iEstatus;
    private String esMensaje;
    private String ipParam;
    private int lvlInternet;
    private String Version = BuildConfig.VERSION_NAME;
    private String versionNameApks = "";
    private String versionCodeApks = "";
    private String ipOffline = "";
    private boolean estabaActivo = true;

    private String MensajeLog = "Default";
    private String EstatusLog = "MenuMovilAforeCoppel - LoginActivity Version:" + Version;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new SesionModuloManager(getApplicationContext());

        Date sysDate = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
        todayDate = dateFormat.format(sysDate);

        queue = Volley.newRequestQueue(this);

        setComponentes();
        setListeners();

        //TODO QUITAR CUANDO YA FUNCIONE EL SERVICIO
        //handleSSLHandshake(); //ELMINAR UNA VEZ QUE SE TENGA LA URL DEL APIREST

        //UNA VEZ QUE ESTAN LOS COMPONENTES SE PIDE PERMISO
        if (hasPermissions(PERMISSIONS) && android.os.Build.VERSION.SDK_INT >= 23) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
        }

        createConfigFile();
        tvVersion.setText("V " + Version);
        btnLogin.setEnabled(true);
        ObtenerVersionesApks();

    }


    public void ObtenerVersionesApks(){
        PackageInfo pinfo;
        String[] packapks = {PAQMOD,PAQREE,PAQFIR,PAQDIG,PAQAFI,PAQVID, PAQDIGAFIL,PAQFIRAFIL,PAQZNPRO};
        String[] NombresApks = {NAME_MODULO+"\t\t",NAME_REENROLAMIENTO+"\t",NAME_FIRMA_ENROLA+"\t\t\t\t\t\t\t",NAME_DIGITALIZADOR+"\t\t",NAME_AFILIACION+"\t\t",NAME_VIDEO+"\t\t\t\t\t", NAME_DIGITALIZADORAFIL+"\t\t\t\t", NAME_FIRMA_ENROLAAFIL+"\t\t\t\t\t", NAME_ZONA+"\t\t\t\t"};
        PackageManager pm = getBaseContext().getPackageManager();
        ArrayList<String> arraySpinner = new ArrayList<String>();

        for (int i = 0; i < packapks.length; i++){
            try {
                if(isPackageInstalled(packapks[i], pm)) {
                    writeLog(NombresApks[i]);
                    pinfo = getPackageManager().getPackageInfo(packapks[i], 0);
                    arraySpinner.add(NombresApks[i] + "\t" + pinfo.versionName);
                    if (i == 0) {
                        versionNameApks += pinfo.versionName;
                        versionCodeApks += pinfo.versionCode + "";
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                writeLog("VERSIONES: "+e.getMessage());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerL.setAdapter(adapter);
    }

    //TODO QUITAR CUANDO YA FUNCIONE EL SERVICIO
    //PERMITIR SOLO DURANTE DESARROLLO EN LO QUE ENTREGAN LA URL
    @SuppressLint({"TrulyRandom","TrustAllX509TrustManager", "BadHostnameVerifier"})
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }


                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((arg0, arg1) -> true);
        } catch (Exception ignored) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public String getIp(){
        int ip;

        UsbManager m = (UsbManager)getApplicationContext().getSystemService(USB_SERVICE);
        HashMap<String, UsbDevice> devices = m.getDeviceList();
        Collection<UsbDevice> ite = devices.values();
        UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});

        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        ip = wifiInfo.getIpAddress();

        @SuppressWarnings("deprecation")
        String IpOrigen = Formatter.formatIpAddress(ip);


        return IpOrigen;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == PERMISSION_ALL && android.os.Build.VERSION.SDK_INT >= 23){
            for(int i=0;i<grantResults.length;i++){
                if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    if(shouldShowRequestPermissionRationale(permissions[i])){
                        AlertDialog mensaje = new AlertDialog.Builder(this)
                                .setMessage("Es necesario que otorgue los permisos para continuar.")
                                .setPositiveButton("Aceptar", (dialog, which) -> requestPermissions(PERMISSIONS, PERMISSION_ALL))
                                .create();
                        mensaje.setCanceledOnTouchOutside(false);
                        mensaje.show();
                    }else{
                        new AlertDialog.Builder(this)
                                .setMessage("Se mostrarán las opciones de la aplicación para que otorgue los permisos necesarios.")
                                .setPositiveButton("Aceptar", (dialog, which) -> darPermiso())
                                .create().show();
                    }
                    return;
                }
            }
        }
    }

    private void darPermiso(){
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void setComponentes(){
        //COMPONENTE PARA INICIO DE SESIÓN
        txtEmpleado = findViewById(R.id.txt_numempleado);
        btnLogin    = findViewById(R.id.btn_iniciar);

        //COMPONENTES PARA MOSTRAR ERROR
        lytNum = findViewById(R.id.txt_numempleado_layout);
        pbCargando = findViewById(R.id.ly_cargando);
        spinnerL = findViewById(R.id.spinner2);
        //TextView para la version
        tvVersion = findViewById(R.id.tvVersion);
    }

    private void setListeners(){
        /* ON CLICK LISTENER */
        btnLogin.setOnClickListener(this);

        /* TextWatcher */
        txtEmpleado.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String sNum = txtEmpleado.getText().toString();

                if(sNum.length() != 0){

                    if(sNum.length() > 7) {
                        lytNum.setError(null);
                        fEmp = true;
                    }
                    else{
                        lytNum.setError("Favor de ingresar un número de empleado valido");
                        fEmp = false;
                    }
                }
                else{
                    lytNum.setError(null);
                    fEmp = false;
                }
            }
        });


        /*
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start,
                                       int end, Spanned dest, int dstart, int dend) {

                for (int i = start;i < end;i++) {
                    if (txtEmpleado.getText().toString().length() < 8) {
                        if(!Character.isDigit(source.charAt(i))){
                            txtEmpleado.setText(txtEmpleado.getText()+"");
                            txtEmpleado.setSelection(txtEmpleado.getText().length());
                            return null;
                        }
                    }else if(txtEmpleado.getText().toString().length() < 12){
                        if(txtEmpleado.getText().toString().length() % 2 == 0){
                            if(!Character.toString(source.charAt(i)).equals("-")){
                                    return "";
                            }
                        }else if(!Character.toString(source.charAt(i)).equals("D")&&
                                 !Character.toString(source.charAt(i)).equals("S")&&
                                 !Character.toString(source.charAt(i)).equals("T")&&
                                 !Character.toString(source.charAt(i)).equals("R")){
                                    return "";
                        }
                    }else{
                        return "";
                    }
                }
                return null;
            }
        };

        txtEmpleado.setFilters(new InputFilter[] { filter });
        */
    }



    private boolean hasPermissions(String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            for (String permission : permissions) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onClick(View v){
        if( v.getId() == btnLogin.getId() ){
            if(hasPermissions(PERMISSIONS)){
                new AlertDialog.Builder(this)
                        .setMessage("Se mostrarán las opciones de la aplicación para que otorgue los permisos necesarios.")
                        .setPositiveButton("Aceptar", (dialog, which) -> darPermiso())
                        .create().show();
            }else if(fEmp) {
                makeDirReen();
                //iEmpleado = Integer.parseInt(txtEmpleado.getText().toString().substring(0,8));
                iEmpleado = Integer.parseInt(txtEmpleado.getText().toString());
                if (iEmpleado > 9999999) {
                    getConfig();
                    WifiManager wifiManager = (WifiManager)this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    if(!wifiManager.isWifiEnabled()){
                        wifiManager.setWifiEnabled(true);
                    }
                    soapVerificarVersiones();
                }
            }
        }
    } //onClick

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        writeLog(" onActivityResult " + data);
        if(requestCode == LLAMAR_LECTOR_HUELLAS){
            if(resultCode == RESULT_OK) {
                cargando(pbCargando, true);
                //LLAMA AL SERVICIO PARA GENERAR LOS TEMPLATES DE LAS HUELLAS LEIDAS
                fromHuellasToTemplate(data);
            }
            else if(resultCode == RESULT_CANCELED){
                cargando(pbCargando, false);
                showToast(this,
                        "Se cancelo la lectura de huella, favor de volver a intentarlo.");
            }
        }
    }


    // MÉTODO QUE SE MANDA LLAMAR CUANDO SE OBTUVO RESPUESA DEL SERVICIO
    @Override
    public void onTaskComplete(String xmlMen, int id){
        MessageDialog aviso;

        if(xmlMen.equals("")){
            cargando(pbCargando, false);
            showToast(this,
                    "Ocurrió un problema durante el proceso, favor de volver a intentar.");
            return;
        }

        HashMap<String, String> values = getValuesXML(xmlMen);

        if(values == null){
            cargando(pbCargando, false);
            showToast(this, "Error al procesar la respuesta del servicio");
            return;
        }

        if(flag){
            iEstatus = Integer.parseInt(values.get("Estatus"));
            esMensaje = values.get("EsMensaje");
        }
        //SE VALIDA DE CUAL LLAMADO VUELVE EL WEBSERVICE

        switch (id) {

            case SOAP_VERIFICAR_VERSIONES:
                    int Numapks = 0;
                    writeLog("VALUES: "+values.toString());
                try {
                    Numapks = Integer.parseInt(values.get("NumApks"));
                }catch(Exception e){
                    writeLog("ERROR: "+e.getMessage());
                }

                if(Numapks == 0){
                    llamaComponenteHuellas();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle("Aviso");
                    builder.setNegativeButton("Aceptar",null);
                    builder.setMessage(R.string.apks_actualizadas);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
            case SOAP_OBTENER_IP:

                ipParam = values.get("ip");

                soapComparacionTemplate();

                break;
            case SOAP_COMPARACION_TEMPLATE:

                respuestaCompara(values);

                break;
            case SOAP_TEMPLATES_EMPLEADO:

                if(iEstatus != 200 && iEstatus != 201){
                    MensajeLog = "Error SOAP_TEMPLATES_EMPLEADO Estatus:"+iEstatus;
                    soapGuardarLogs();
                    cargando(pbCargando, false);
                    aviso = new MessageDialog();
                    aviso.setDefaultAceptar(esMensaje);
                    showMessage(this, aviso);
                    return;
                }

                JSONObject pEntrada = new JSONObject();
                JSONArray fingers = new JSONArray();

                JSONObject fing;

                for(int i=0; i < 10; i++){
                    try {
                        fing = new JSONObject();
                        fing.put("FingerId", (i+1) + "");
                        fing.put("Template", values.get("Fingerid" + (i+1)));
                        fingers.put(i, fing);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                soapConsultarOffline();

                //TODO
                File jsonPath = new File(DIR_ACTUAL, "templateEmp" + todayDate + ".json");
                String fileContent = readFile(jsonPath.getAbsolutePath());

                try {
                    JSONObject lector = new JSONObject(fileContent);
                    JSONObject dedo = new JSONObject(lector.getJSONArray("FingerTemplate").get(0).toString());

                    fing = new JSONObject();
                    fing.put("FingerId", "11");

                    template = dedo.getString("Template");

                    fing.put("Template", template);
                    fingers.put(10, fing);

                    pEntrada.put("NumberEmp", iEmpleado + "");
                    pEntrada.put("FingerTemplate", fingers);

                    File jsonCompare = new File(DIR_ACTUAL,  "compareTempEmp.json");
                    File jsonCompareAfil = new File(DIR_ACTUALAFIL,  "compareTempEmp.json");
                    writeFile(jsonCompare, pEntrada.toString().getBytes(), false);
                    writeFile(jsonCompareAfil, pEntrada.toString().getBytes(), false);

                } catch (JSONException e) {
                    cargando(pbCargando, false);
                    showToast(this, "Error en la respuesta del servidor, favor de intentar de nuevo.");
                    MensajeLog = "Error en el try catch SOAP_TEMPLATES_EMPLEADO";
                    soapGuardarLogs();
                }

                soapObtenerIp();

                break;
            case SOAP_VALIDAR_EMPLEADO:
                cargando(pbCargando, false);

                if(iEstatus == 200){
                    String fecha = values.get("FechaServidor");
                    Date sysDate = Calendar.getInstance().getTime();
                    DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String todayTime = timeFormat.format(sysDate);

                    if(fecha != null){
                        if(fecha.equals(todayTime)){
                            session.createLoginSession(fecha,  values.get("Empleado"),  values.get("NombreProm"),
                                    values.get("ApPatProm"),   values.get("ApMatProm"), values.get("NombreCentro"),
                                    values.get("ClaveConsar"), values.get("CurpProm"),  values.get("Contrasenia"),
                                    values.get("PassSaftv"),   values.get("Tienda"),    values.get("Centro"));

                            writeLog("Clave Consar: "+values.get("ClaveConsar"));
                            //Staring MenuActivity

                            Intent i = new Intent(getApplicationContext(), MenuActivity.class);
                            Bundle extra = new Bundle();
                            extra.putInt(IDENT_IP, identificadorParam);
                            extra.putString("ipOffline", "http://" + ipOffline);
                            i.putExtras(extra);
                            startActivity(i);
                        }
                        else{
                            aviso = new MessageDialog();
                            aviso.setDefaultAceptar("La fecha del dispositivo no coincide con la fecha " +
                                    "del servidor [" + fecha + "], favor de verificar la fecha.");
                            showMessage(this, aviso);
                        }
                    }
                    else{
                        aviso = new MessageDialog();
                        aviso.setDefaultAceptar("Ocurrió un error al obtener la fecha del servidor, favor de volverlo a intentar.");
                        showMessage(this, aviso);

                        MensajeLog = "La fecha es null";
                        soapGuardarLogs();
                    }

                }else if(iEstatus == 403){
                    MensajeLog = "Estatus Incorrecto: "+iEstatus;
                    soapGuardarLogs();
                    aviso = new MessageDialog();
                    aviso.setDefaultAceptar(esMensaje);
                    showMessage(this, aviso);
                }

                break;
            case SOAP_OBTENER_IP_OFFLINE:

                ipOffline = values.get("ip");
                ipOffline = ipOffline.trim();
                break;
        }

    }

    public void makeDirReen(){
        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
        File dirReenrol = new File(DIR_ACTUAL);
        if(!dirReenrol.exists())
            if(dirReenrol.mkdir()){
                File dirTemp = new File(DIR_ACTUAL + DIR_TEMP);
                if(!dirTemp.exists())
                    if(!dirTemp.mkdir())
                        showToast(this, "Ocurrió un error al configurar la aplicación, favor de reiniciar");
            }
        DIR_ACTUALAFIL = DIR_EXTERNA + DIR_AFILIACION;
        File dirAfiliacion = new File(DIR_ACTUALAFIL);
        if(!dirAfiliacion.exists())
            if(dirAfiliacion.mkdir()){
                File dirTemp = new File(DIR_ACTUALAFIL + DIR_TEMP);
                if(!dirTemp.exists())
                    if(!dirTemp.mkdir())
                        showToast(this, "Ocurrió un error al configurar la aplicación, favor de reiniciar");
            }
    }

    private void llamaComponenteHuellas(){
        /*LLAMADO COMPONENTE DE HUELLAS*/
        Intent intent = new Intent(LoginActivity.this,MainActivityEnrollMovil.class);
        //intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

        Bundle extra = new Bundle();
        extra.putInt(ACT_HUELLAS_OPC, 4);
        extra.putInt(ACT_HUELLAS_NUM, iEmpleado);
        extra.putInt(ACT_HUELLAS_IMG, 1);
        intent.putExtras(extra);

        //startActivityForResult(intent,100);
        checkIntentAndStart(intent, 100, NAME_LECTOR_HUELLAS);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void fromHuellasToTemplate(Intent data){

        MessageDialog errorMsn = new MessageDialog();
        int     OUT_1 = -1;
        String  OUT_2 = "";// OUT_3 = "", OUT_4 = "";

        Bundle resApp = data.getExtras();
        if(resApp != null) {
            OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);
            OUT_2 = resApp.getString (ACT_HUELLAS_OT2);
            //OUT_3 = resApp.getString (ACT_HUELLAS_OT3);
            //OUT_4 = resApp.getString (ACT_HUELLAS_OT4);
        }

        switch (OUT_1) {
            case 1: //EXITO EN LA TOMA DE HUELLAS
                JSONObject param = new JSONObject();
                String Maker, Model, Serial, Firmware, IpOrigen, ruta;

                try {

                    ruta = DIR_EXTERNA+"sys/mem/"+iEmpleado+"/JsonEnviado_"+iEmpleado+".txt";
                    OUT_2 = readFile(ruta).trim();

                    writeLog("RUTA: "+ruta);

                    JSONObject respLector = new JSONObject(OUT_2);
                    JSONArray arrayEx = respLector.getJSONArray("Fingerprints");
                    Maker = respLector.getString("Maker");
                    Model = respLector.getString("Model");
                    Serial = respLector.getString("Serial");
                    Firmware = respLector.getString("Firmware");
                    IpOrigen = getIp();


                    param.put("NumberEmp", JSONObject.numberToString(iEmpleado));
                    param.put("EmpAutoriza", JSONObject.numberToString(iEmpleado));
                    param.put("IpOrigen", IpOrigen);
                    param.put("Fingerprints", arrayEx);
                    param.put("FingersException", respLector.get("FingersException"));
                    param.put("Maker", Maker);
                    param.put("Model", Model);
                    param.put("Serial", Serial);
                    param.put("Firmware", Firmware);

                   File jsonEntrada = new File(DIR_ACTUAL + "jsonEntrada.json");
                   writeFile(jsonEntrada, param.toString().getBytes(), false);

                } catch (JSONException e) {
                    cargando(pbCargando, false);
                    showToast(this, "Ocurrió un error, favor de intentar de nuevo.");
                    writeLog(e.getMessage());
                    writeLog(e.toString());
                }

                String url;
                if (Utilerias.existeConexionWiFi(this, lvlInternet)) {
                    url = RESTWIFI + REST_IMGE;
                }else{
                    url = RESTDATOS + REST_IMGE;
                }

                /*  writeLog(" url " + url);

                writeLog("param" + param);*/

                lvlInternet = 0;
                writeLog("Nivel de Internet: "+lvlInternet);
                //GENERAR TEMPLATES
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.POST, url, param, this::obtenerTemplatesBD,
                                error -> {
                            cargando(pbCargando, false);
                            //String log = error.toString() + " " + error.networkResponse + " " + error.getMessage() + " " + error.getLocalizedMessage();
                            //writeFile(new File(DIR_ACTUAL + "error.json"),log.getBytes(), false);
                            MensajeLog = "Error en llamado a ApiRest  Error: "+error.toString()+"URL: "+url+" | Param: "+param.toString();
                            showToast(this,
                                    "Ocurrió un error al procesar la solicitud, favor de revisar su " +
                                            "conexión de internet, si el problema persiste comuniquese a mesa de ayuda.");
                        });

                queue.add(jsObjRequest);

                break;

            case 99: //PROCESO CANCELADO
                cargando(pbCargando, false);
                showToast(this,
                        "Se cancelo la lectura de huella, favor de volver a intentar.");
                break;
            case 0:
                cargando(pbCargando, false);
                showToast(this,
                        "Ocurrio un error en la captura de huellas, favor de volver a intentar.");
                break;
            case 2:
                cargando(pbCargando, false);
                showToast(this,
                        "No se pudieron almacenar las imágenes.");
                break;
            default: //ERROR INESPERADO
                cargando(pbCargando, false);
                showToast(this,
                        "Ocurrio un error inesperado en la captura de huellas, favor de volver a intentar.");
                break;
        }
    }

    private void obtenerTemplatesBD(JSONObject response) {
        try {

            int apiEstatus = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            int apiResultado = response.getInt(REST_RP_RES);

            if (apiEstatus == 201 && apiResultado == 0) {

                //showToast(this, apiMensaje);

                File jsonResp = new File(DIR_ACTUAL, "templateEmp" + todayDate + ".json");
                writeFile(jsonResp, response.toString().getBytes(), false);

                //MANDAR LLAMAR EL WS PARA OBTENER LOS TEMPLATES DEL PROMOTOR
                soapObtenerTemplatesProm();

            } else {
                cargando(pbCargando, false);
                showToast(this,
                        "Ocurrió un error al realizar la conexión al servidor, favor de volver a intentar.");
                MensajeLog = "ApiEstatus: "+apiEstatus+" | ApiResultado: "+apiResultado;
                soapGuardarLogs();
            }

        } catch (JSONException e) {
            cargando(pbCargando, false);
            showToast(this,
                    "Ocurrió un error al realizar la conexión al servidor, favor de revisar su conexión a internet.");
            MensajeLog = "Error En try catch ObtenerTemplatesBD";
            soapGuardarLogs();
        }
    }

    private void respuestaCompara(HashMap<String, String> response){
            //int apiResultado  = response.getInt(REST_RP_RES);

        writeLog("respuestaCompara");
        int IdDedo = 0;
        String Mensaje = "";
            IdDedo = Integer.parseInt(response.get("lEstado"));
            Mensaje = response.get("cDescripcion");

        writeLog("respuestaCompara "+IdDedo+" "+Mensaje);

                if( IdDedo > 0){
                    soapObtenerDatosProm();
                }else{
                    MensajeLog = "IdDedo: "+IdDedo+" Mensaje: "+Mensaje;
                    soapGuardarLogs();
                    cargando(pbCargando, false);
                    showToast(this, "La huella no corresponde");
                }
    }

    //PRIMER PETICIÓN AL WS - OBTIENE LOS TEPLATES DEL PROMOTOR
    private void soapObtenerTemplatesProm(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "ObtenerTemplatesPromotor");
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("iEmpleado", iEmpleado + "");
        sSoap.setParams("identificador",identificadorParam+"");
        sSoap.setParams("cVersion",EstatusLog+ "Numero de empleado: "+iEmpleado);
        flag = true;

        CallWebService callWS = new CallWebService(this, SOAP_TEMPLATES_EMPLEADO);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapComparacionTemplate(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        writeLog("ipParam.trim() " + ipParam.trim());
        writeLog("iipAddressWsH " + ipAddressWsH);
        ipParam = ipParam.replace("10.27.47.12","10.44.172.234");
        new CallWebService(ipAddressWsH);
        SoapString sSoap = new SoapString("wsHuellasHC",
                "AforeComparacionTemplate");
        sSoap.setParams("lNumero", iEmpleado+"");
        sSoap.setParams("cTemplate",template );
        sSoap.setParams("iSizeTemplate",template.length()+"");
        sSoap.setParams("cIpAfore",ipParam.trim());
        writeLog("ipParam.trim() " + ipParam.trim());
        sSoap.setParams("iTipoPersona","1");

        flag = false;

        CallWebService callWS = new CallWebService(this, SOAP_COMPARACION_TEMPLATE);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapObtenerIp(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "ObtenerDatoIp");
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("identificador",iEmpleado+"");
        sSoap.setParams("iTabla", "2");
        sSoap.setParams("cVersion",EstatusLog+ "Numero de empleado: "+iEmpleado);
        flag = false;

        CallWebService callWS = new CallWebService(this, SOAP_OBTENER_IP);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }


    private void soapObtenerDatosProm(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "ValidaEnrolamientoPromotor");

        String macaddress = getMacAddr();

        if(!macaddress.equals("")){
            sSoap.setParams("apikey", apikey);
            sSoap.setParams("macaddress", macaddress);
            sSoap.setParams("iEmpleado", iEmpleado + "");
            sSoap.setParams("identificador",identificadorParam+"");
            sSoap.setParams("cVersion",EstatusLog+ "Numero de empleado: "+iEmpleado);

            flag = true;

            CallWebService callWS = new CallWebService(this, SOAP_VALIDAR_EMPLEADO);
            callWS.setFlag(true);
            callWS.execute(sSoap.getSoapString());
        }
        else{
            cargando(pbCargando, false);
            showToast(this,
                    "Ocurrió un error al realizar la conexión al servidor, favor de volver a intentar.");
            MensajeLog = "Error al obtener la Macaddress";
            soapGuardarLogs();
            MessageDialog aviso;
            aviso = new MessageDialog();
            aviso.setTitle("Aviso");
            aviso.setDefaultAceptar("Promotor ocurrio un error al obtener la Mac Address, favor de comunicarse con Mesa de Ayuda.");
            showMessage(this, aviso);
            return;
        }
    }

    private void soapGuardarLogs(){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog+ "Numero de empleado: "+iEmpleado);

        flag = true;

        CallWebService callWS = new CallWebService(this,0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapConsultarOffline(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "ObtenerDatoIp");
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("identificador",iEmpleado+"");
        sSoap.setParams("iTabla", "1");
        sSoap.setParams("cVersion",EstatusLog+ "Numero de empleado: "+iEmpleado);

        flag = false;

        CallWebService callWS = new CallWebService(this, SOAP_OBTENER_IP_OFFLINE);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapVerificarVersiones(){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "VerificarVersiones");
        writeLog("ENTRO soapVerificarVersiones");
        sSoap.setParams("cVersionName", versionNameApks);
        sSoap.setParams("cVersionCode", versionCodeApks);
        sSoap.setParams("cVersionesapks", MODULO+"");
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("cVersion",EstatusLog+ "Numero de empleado: "+iEmpleado);

        flag = true;
        CallWebService callWS = new CallWebService(this,SOAP_VERIFICAR_VERSIONES);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void createConfigFile(){

        JSONObject object = new JSONObject();
        File file = new File(getExternalFilesDir(null), CONFIG_NAME);

        try {
            object.put(CONFIG_KEY1, CONFIG_KEY2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(!file.exists())
            if(!writeFile(file, object.toString().getBytes(), false)){
                MessageDialog aviso = new MessageDialog();
                aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                        "es necesario reiniciarla.");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
                showMessage(this, aviso);
            }
    }

    public void getConfig(){
            File file = new File(getExternalFilesDir(null), CONFIG_NAME);
            String key = "";
        try {
            JSONObject json = new JSONObject(readFile(file.getAbsolutePath()));
            writeLog(file.getAbsolutePath());
            writeLog(json.toString());
            key = json.getString(CONFIG_KEY1);
        } catch (JSONException e) {
            MessageDialog aviso = new MessageDialog();
            aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                    "es necesario reiniciarla.");
            aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
            showMessage(this, aviso);
        }

        writeLog("key: "+key);

        switch(key){
            case CONFIG_KEY2:
                writeLog("ENTRO en CONFIG_KEY2 " + CONFIG_KEY2);
                //ipAddress = BuildConfig.AmbientePro;
                //ipAddressWsH = "http://www.aforecoppel.com:2097";
                //Se coloca de acuerdo a la conexión realizada.
                if (Utilerias.existeConexionWiFi(this, lvlInternet)) {
                    ipAddress = "http://10.26.190.158:20307";  /* "http://www.aforecoppel.com:2098" */
                    ipAddressWsH = "http://10.26.190.170:20616";
                    //ipAddressWsH = "http://10.26.190.158:2097";
                    //ipAddressWsH = "http://10.26.190.170:20616";
                }else{
                    ipAddress = BuildConfig.AmbientePro;  /* "http://www.aforecoppel.com:2098" */
                    ipAddressWsH = "http://www.aforecoppel.com:2097";
                }


                break;
            case CONFIG_KEY3:
                writeLog("ENTRO en CONFIG_KEY3 " + CONFIG_KEY3);
                ipAddress = BuildConfig.AmbienteDes;
                ipAddressWsH = "http://10.44.172.234:20616";

                break;
            case CONFIG_KEY4:
                writeLog("ENTRO en CONFIG_KEY4 " + CONFIG_KEY4);
                ipAddress = BuildConfig.AmbienteTes;
                ipAddressWsH = "http://10.27.142.242:20616";


                break;
            case CONFIG_KEY5:
                writeLog("ENTRO en CONFIG_KEY5 " + CONFIG_KEY5);
                ipAddress = "http://10.27.142.223:20307";
                ipAddressWsH = "http://10.27.142.208:20616";
                break;
            default: //EL ARCHIVO SE MODIFICO CON VALORES INCORRECTOS
                MessageDialog aviso = new MessageDialog();
                aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                        "es necesario reiniciarla.");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
                showMessage(this, aviso);
                break;
        }

        new CallWebService(ipAddress);
    }

   /* public void getConfigExisteWiFi(){
        String key = "";
        try {
            JSONObject json = new JSONObject(readFile(file.getAbsolutePath()));
            writeLog(file.getAbsolutePath());
            writeLog(json.toString());
            key = json.getString(CONFIG_KEY1);
        } catch (JSONException e) {
            MessageDialog aviso = new MessageDialog();
            aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                    "es necesario reiniciarla.");
            aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
            showMessage(this, aviso);
        }
        switch(key){
            case CONFIG_KEY2:
                //ipAddress = BuildConfig.AmbientePro;
                //ipAddressWsH = "http://www.aforecoppel.com:2097";
                //Se coloca de acuerdo a la conexión realizada.

                writeLog("Garza: Inicia Utilerias.existeConexionWiFi ");
                if (Utilerias.existeConexionWiFi(this)) {
                    ipAddress = "http://10.26.190.158:20307";
                    ipAddressWsH = "http://10.26.190.170:20616";
                }else{
                    ipAddress = BuildConfig.AmbientePro;  /* "http://www.aforecoppel.com:2098"
                    ipAddressWsH = "http://www.aforecoppel.com:2097";
                }

                break;
            case CONFIG_KEY3:
                ipAddress = BuildConfig.AmbienteDes;
                ipAddressWsH = "http://10.44.172.234:20616";

                break;
            case CONFIG_KEY4:
                ipAddress = BuildConfig.AmbienteTes;
                ipAddressWsH = "http://10.27.142.242:20616";


                break;
            default: //EL ARCHIVO SE MODIFICO CON VALORES INCORRECTOS
                MessageDialog aviso = new MessageDialog();
                aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                        "es necesario reiniciarla.");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
                showMessage(this, aviso);
                break;
        }

          writeLog("GarFFza: Termina getConfigExisteWiFi LA");
 //       new CallWebService(ipAddress,Constantes.IP_ADDRESS);
    }*/

    private void checkIntentAndStart(Intent intent, int idActivity, String application){
        PackageManager manager = this.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            startActivityForResult(intent, idActivity);
        } else {
            MessageDialog aviso = new MessageDialog();
            aviso.setTitle("Falta de aplicación");
            aviso.setDefaultAceptar("Es necesario instalar la aplicación: <b>" + application + "<b>");
            showMessage(this, aviso);
        }
    }

    @Override
    public void onBackPressed() {   // SE SOBREESCRIBE EL MÉTODO DEL BOTÓN REGRESAR
        finishAffinity();
        finish();
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
