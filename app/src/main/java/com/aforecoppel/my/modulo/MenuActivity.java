package com.aforecoppel.my.modulo;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.aforecoppel.my.modulo.Utilerias.*;

public class MenuActivity extends AppCompatActivity implements AsyncTaskCompleteListener<String>,
        View.OnClickListener{

    // CLASE PARA EL MANEJO DE SESIÓN
    private  SesionModuloManager session;

    // OBJETOS DE LA ACTIVIDAD
    private AppCompatImageButton btnReenrol, btnAfiliacion, btnZonaPromotores, btnLiga;
    private TextView txtName;
    private TextView txtCentro;
    private TextView tvVersion;
    public String ipAddress;
    private int identificador;
    public String ipwshuellashc = "";
    private String MensajeLog = "Default";
    private String Version = BuildConfig.VERSION_NAME;
    private String EstatusLog = "MenuMovilAforeCoppel - MenuActivity Version:"+Version;
    private Spinner spinnerM;
    private String iEmpleado;
    private String ipOffline = "";
    private String [] sApksVerName;
    private int [] sApksVerCode;
    private int ID_APK = 0;
    private final String apikey = BuildConfig.ApiKeyModul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SesionModuloManager(getApplicationContext());
        session.checkLogin(); // REVISAR SI EXISTE UNA SESIÓN

        setContentView(R.layout.activity_menumovil);
        setComponentes();
        setListeners();

        Intent intent = getIntent();
        getParameters(intent);

        // OBTENER LA INFORMACIÓN DE USUARIO DE LA SESIÓN
        String nomCompleto  = session.getUserFullName();
        String nomCentro    = session.getNumCentro() + " " + session.getNomCentro();
        iEmpleado = session.getNumberEmp();



        // SE ASIGNAN LOS VALORES A LAS ETIQUETAS CORRESPONDIENTES PARA MOSTRAR LA INFORMACIÓN
        txtName.setText(nomCompleto);
        txtCentro.setText(nomCentro.trim());
        tvVersion.setText("V "+BuildConfig.VERSION_NAME);
        if(session.getNomCentro().equals("")){
            MessageDialog aviso = new MessageDialog();
            aviso.setTitle("Aviso");
            aviso.setDefaultAceptar("Mensaje Informativo: Al finalizar todo el trámite, reporta a mesa de ayuda que no tienes un centro asignado en el sistema de Re Enrolamiento Móvil.");
            showMessage(this, aviso);
        }

        getConfig();
        ObtenerVersionesApks();
        identificador = getIntent().getIntExtra(IDENT_IP, 0);

        if(session.getClaConsar().equals("") || session.getClaConsar().equals("0000000000") || session.getClaConsar().equals("0")){
            btnReenrol.setEnabled(false);
            btnReenrol.setAlpha(.5f);

            MessageDialog aviso = new MessageDialog();
            aviso.setTitle("Aviso");
            aviso.setDefaultAceptar("Promotor: No has concluido el trámite de revalidación. Es necesario que se concluya para tener el acceso normal.");
            showMessage(this, aviso);
        }else{
            btnReenrol.setEnabled(true);
            btnReenrol.setAlpha(1f);
        }

    }


    public void getParameters(Intent intent){

           ipOffline = intent.getStringExtra("ipOffline");

    }


    public void ObtenerVersionesApks(){
        PackageInfo pinfo;
        String[] packapks = {PAQMOD,PAQREE,PAQFIR,PAQDIG,PAQAFI,PAQVID, PAQDIGAFIL,PAQFIRAFIL,PAQZNPRO};
        String[] NombresApks = {NAME_MODULO+"\t\t",NAME_REENROLAMIENTO+"\t",NAME_FIRMA_ENROLA+"\t\t\t\t\t\t\t",NAME_DIGITALIZADOR+"\t\t",NAME_AFILIACION+"\t\t",NAME_VIDEO+"\t\t\t\t\t", NAME_DIGITALIZADORAFIL+"\t\t\t\t", NAME_FIRMA_ENROLAAFIL+"\t\t\t\t\t", NAME_ZONA+"\t\t\t\t"};
        PackageManager pm = getBaseContext().getPackageManager();
        ArrayList<String> arraySpinner = new ArrayList<String>();

        sApksVerName = new String[packapks.length];
        sApksVerCode = new int[packapks.length];

        for (int i = 0; i < packapks.length; i++){
            try {
                if(isPackageInstalled(packapks[i], pm)) {
                    pinfo = getPackageManager().getPackageInfo(packapks[i], 0);
                    arraySpinner.add(NombresApks[i] + "\t" + pinfo.versionName);
                    sApksVerName[i] = pinfo.versionName;
                    sApksVerCode[i] = pinfo.versionCode;
                }else{
                    sApksVerName[i] = "0";
                    sApksVerCode[i] = 0;
                }
            } catch (PackageManager.NameNotFoundException e) {
                writeLog("VERSIONES: "+e.getMessage());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerM.setAdapter(adapter);

        soapConsultarOffline();

    }
    // SE SETTEAN LOS COMPONENTES A SUS OBJETOS CORRESPONDIENTES

    public void setComponentes(){
        txtName = findViewById(R.id.txt_name);
        txtCentro = findViewById(R.id.txt_tienda);
        btnReenrol = findViewById(R.id.btn_reenrol);
        tvVersion = findViewById(R.id.tvVersionM);
        spinnerM = findViewById(R.id.spinner3);
        btnAfiliacion = findViewById(R.id.btn_afiliacion);
        btnZonaPromotores = findViewById(R.id.btn_zona_promotores);
        btnLiga = findViewById(R.id.btn_ligas);
    }
    // SE ASIGNAN LOS LISTENERS A LOS OBJETOS QUE LOS NECESITEN

    public void setListeners(){
        /*ON CLICK LISTENER*/
        btnReenrol.setOnClickListener(this);
        btnAfiliacion.setOnClickListener(this);
        btnZonaPromotores.setOnClickListener(this);
        btnLiga.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {   // SE SOBREESCRIBE EL MÉTODO DEL BOTÓN REGRESAR
        MessageDialog aviso = new MessageDialog();
        aviso.setTitle("Cerrar Sesión");
        aviso.setDefaultAceptarCancelar("Promotor, ¿Esta seguro que desea cerrar sesión?",
                (dialog, which) -> soapFinalizarSesion());
        showMessage(this, aviso);
    }

    @Override
    public void onClick(View view) {
        makeDirReen();
        if(view.getId() == btnReenrol.getId()){
            ID_APK = REEN;
            soapVerificarVersiones(sApksVerName[1]+","+sApksVerName[2]+","+sApksVerName[3],
                                   sApksVerCode[1]+","+sApksVerCode[2]+","+sApksVerCode[3],
                                         REEN+","+FIRMA+","+DIGITALIZADOR);
        }
        if(view.getId() == btnAfiliacion.getId()){
            ID_APK = AFILIACION;
            soapVerificarVersiones(sApksVerName[4]+","+sApksVerName[5]+","+sApksVerName[6]+","+sApksVerName[7],
                                   sApksVerCode[4]+","+sApksVerCode[5]+","+sApksVerCode[6]+","+sApksVerCode[7],
                                         AFILIACION+","+VIDEOAFORE+","+DIGITALIZADORAFIL+","+FIRMAAFIL);
        }
        if(view.getId() == btnZonaPromotores.getId()){
            llamaComponenteZonaPromtores();
        }
        if(view.getId() == btnLiga.getId()){
            llamaComponenteLigas();
        }
    }

    public void onTaskComplete(String xmlMen, int id) {
        MessageDialog aviso;
        writeLog("xmlMen: "+xmlMen);
        writeLog("id: "+id);

        if(xmlMen.equals("")){
            /*showToast(this,
                    "Promotor, ocurrió un problema durante el proceso, favor de volver a intentar." + id);*/
            return;
        }

        HashMap<String, String> values = getValuesXML(xmlMen);

        if(values == null){
            MensajeLog = "Values: "+values.toString();
            soapGuardarLogs();
            showToast(this, "Error al procesar la respuesta del servicio");
            return;
        }

        int iEstatus = Integer.parseInt(values.get("Estatus"));
        String esMensaje = values.get("EsMensaje");

        if(iEstatus == SOAP_SESION_INACTIVA){
            session.logoutUser();
            return;
        }

        //SE VALIDA DE CUAL LLAMADO VUELVE EL WEBSERVICE
        switch (id) {
            case SOAP_OBTENER_IP:

                break;
            case SOAP_FINALIZAR_SESION:

                if(iEstatus != 200 && iEstatus != 201){
                    aviso = new MessageDialog();
                    aviso.setDefaultAceptar(esMensaje);
                    showMessage(this, aviso);
                    return;
                }

                session.logoutUser();

                break;
            case SOAP_OBTENER_IP_OFFLINE:

                ipOffline = values.get("ip");
                ipOffline = "http://" + ipOffline.trim();
                break;
            case SOAP_VERIFICAR_VERSIONES:
                int Numapks = 0;
                writeLog("VALUES: "+values.toString());
                try {
                    Numapks = Integer.parseInt(values.get("NumApks"));
                }catch(Exception e){
                    writeLog("ERROR: "+e.getMessage());
                }

                if(Numapks == 0){
                    switch (ID_APK){
                        case REEN:
                            llamaComponenteReenrol();
                            break;
                        case AFILIACION:
                            llamaComponenteAfiliacion();
                            break;

                    }
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                    builder.setTitle("Aviso");
                    builder.setNegativeButton("Aceptar",null);
                    builder.setMessage(R.string.apks_actualizadas);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
        }

    }

    public void makeDirReen(){
        String DIR_ACTUAL;
        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
        File dirReenrol = new File(DIR_ACTUAL);
        if(!dirReenrol.exists())
            if(dirReenrol.mkdir()){
                File dirTemp = new File(DIR_ACTUAL + DIR_TEMP);
                File dirLog = new File(DIR_ACTUAL + DIR_LOGS);
                if(!dirTemp.exists())
                    if(!dirTemp.mkdir())
                        showToast(this, "Ocurrió un error al configurar la aplicación, favor de reiniciar");
                if(!dirLog.exists())
                    if(!dirLog.mkdir())
                        showToast(this, "Ocurrió un error al configurar la aplicación, favor de reiniciar");
            }
    }

    private void llamaComponenteReenrol(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(ACT_REENROL_PKG,ACT_REENROL_CLS));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress);
        extra.putString("ipwshuellashc",ipwshuellashc);
        extra.putInt(ACT_REENROL_NUM, Integer.parseInt(session.getNumberEmp()));
        extra.putString(ACT_REENROL_NOM,   session.getUserFullName());
        extra.putString(ACT_REENROL_CEN,   session.getCentro());
        extra.putInt(IDENT_IP,   identificador);
        intent.putExtras(extra);

        checkIntentAndStart(intent, LLAMAR_REENROLAMIENTO, NAME_REENROLAMIENTO);
    }

    private void soapConsultarOffline(){

        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "ObtenerDatoIp");

        final String TokenAccess = getTokenAccess(BuildConfig.ApiKeyModul, true);
        sSoap.setParams("tokenAccess", TokenAccess);
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("identificador",iEmpleado+"");
        sSoap.setParams("iTabla", "1");
        sSoap.setParams("cVersion","MenuMovilAforeCoppel Version_name:"+BuildConfig.
                VERSION_NAME+" Version_code:"+BuildConfig.VERSION_CODE+" Numero de empleado: "+iEmpleado);

        CallWebService callWS = new CallWebService(this, SOAP_OBTENER_IP_OFFLINE);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void llamaComponenteAfiliacion(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(ACT_AFILIACION_PKG,ACT_AFILIACION_CLS));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress);
        extra.putInt(ACT_AFILIACION_NUM, Integer.parseInt(session.getNumberEmp()));
        extra.putString(ACT_AFILIACION_NOM,   session.getUserFullName());
        extra.putString(ACT_AFILIACION_CEN,   session.getCentro());
        extra.putString("ipOffline",   ipOffline);
        extra.putString("ipwshuellashc",   ipwshuellashc);
        intent.putExtras(extra);

        checkIntentAndStart(intent, LLAMAR_AFILIACION, NAME_AFILIACION);
    }

    private void llamaComponenteZonaPromtores(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(ACT_ZONA_PKG,ACT_ZONA_CLS));
        checkIntentAndStart(intent, LLAMAR_ZONA, NAME_ZONA);
    }

    private void soapFinalizarSesion(){
        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        SoapString sSoap = new SoapString("wsmodulomovil",
                "FinalizarSesionPromotor");

        final String TokenAccess = getTokenAccess(BuildConfig.ApiKeyModul, true);
        sSoap.setParams("tokenAccess", TokenAccess);
        sSoap.setParams("identificador", identificador + "");
        sSoap.setParams("cVersion","MenuMovilAforeCoppel Version_name:"+BuildConfig.
                VERSION_NAME+" Version_code:"+BuildConfig.VERSION_CODE+" Numero de empleado: "+iEmpleado);


        CallWebService callWS = new CallWebService(this, SOAP_FINALIZAR_SESION);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapGuardarLogs(){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);
        sSoap.setParams("cVersion","MenuMovilAforeCoppel Version_name:"+BuildConfig.
                VERSION_NAME+" Version_code:"+BuildConfig.VERSION_CODE+" Numero de empleado: "+iEmpleado);

        CallWebService callWS = new CallWebService(this, 0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapVerificarVersiones(String sVerNameApks, String sVerCodeApks, String sVerId){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsmodulomovil",
                "VerificarVersiones");

        sSoap.setParams("cVersionName", sVerNameApks);
        sSoap.setParams("cVersionCode", sVerCodeApks);
        sSoap.setParams("cVersionesapks", sVerId);
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("cVersion","MenuMovilAforeCoppel Version_name:"+BuildConfig.
                VERSION_NAME+" Version_code:"+BuildConfig.VERSION_CODE+" Numero de empleado: "+iEmpleado);


        CallWebService callWS = new CallWebService(this,SOAP_VERIFICAR_VERSIONES);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        //Check which request were responding to
        if(requestCode == LLAMAR_REENROLAMIENTO){
            //Make sure the request was successful
            if(resultCode == RESULT_OK){
                showToast(this, "Reenrolamiento finalizado con exito");
            }
            else if(resultCode == RESULT_CANCELED){
                showToast(this, "Reenrolamiento cancelado");
            }
            else if(resultCode == SOAP_SESION_INACTIVA){
                session.logoutUser();
            }
        }

    }

    public void getConfig(){
        File file = new File(getExternalFilesDir(null), CONFIG_NAME);
        String key = "";
        try {
            JSONObject json = new JSONObject(readFile(file.getAbsolutePath()));
            key = json.getString(CONFIG_KEY1);
        } catch (JSONException e) {
            MessageDialog aviso = new MessageDialog();
            aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                    "es necesario reiniciarla.");
            aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
            showMessage(this, aviso);
        }
        switch(key){
            case CONFIG_KEY2:
                if (Utilerias.existeConexionWiFi(this, 0)) {
                    ipAddress = "http://10.26.190.158:20307";  /* "http://www.aforecoppel.com:2098" */
                    ipwshuellashc = "http://10.26.190.170:20616";
                }else{
                    ipAddress = BuildConfig.AmbientePro;  /* "http://www.aforecoppel.com:2098" */
                    ipwshuellashc = "http://www.aforecoppel.com:2097";
                }
                break;
            case CONFIG_KEY3:
                ipAddress = BuildConfig.AmbienteDes;
                ipwshuellashc = "http://10.44.172.234:20616";
                break;
            case CONFIG_KEY4:
                ipAddress = BuildConfig.AmbienteTes;
                ipwshuellashc = "http://10.27.142.242:20616";
                break;
            case CONFIG_KEY5:
                ipAddress = "http://10.27.142.223:20307";
                ipwshuellashc = "http://10.27.142.208:20616";
                break;
            default: //EL ARCHIVO SE MODIFICO CON VALORES INCORRECTOS
                MensajeLog = "KEY: "+key;
                soapGuardarLogs();
                MessageDialog aviso = new MessageDialog();
                aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                        "es necesario reiniciarla.");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
                showMessage(this, aviso);
                break;
        }
        new CallWebService(ipAddress);
    }

    /*public void getConfigExisteWiFi(){

        writeLog("Garza: Inicia getConfigExisteWiFi MA ");
        File file = new File(getExternalFilesDir(null), CONFIG_NAME);
        String key = "";
        try {
            JSONObject json = new JSONObject(readFile(file.getAbsolutePath()));
            key = json.getString(CONFIG_KEY1);
        } catch (JSONException e) {
            MessageDialog aviso = new MessageDialog();
            aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                    "es necesario reiniciarla.");
            aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
            showMessage(this, aviso);
        }

        switch(key){
            case CONFIG_KEY2:
                ipAddress = BuildConfig.AmbientePro;
                break;
            case CONFIG_KEY3:
                ipAddress = BuildConfig.AmbienteDes;
                break;
            case CONFIG_KEY4:
                ipAddress = BuildConfig.AmbienteTes;
                break;
            default: //EL ARCHIVO SE MODIFICO CON VALORES INCORRECTOS
                MessageDialog aviso = new MessageDialog();
                aviso.setMensaje("Ocurrió un error al realizar las configuraciones de la aplicación, " +
                        "es necesario reiniciarla.");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finishAndRemoveTask());
                showMessage(this, aviso);
                break;
        }
 //       new CallWebService(ipAddress, Constantes.IP_ADDRESS);
    }*/


    private void llamaComponenteLigas(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(PAQMOD,ACT_AFILIACION_ENLACE_CLS));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress.replace("20307","50131"));
        extra.putInt(ACT_AFILIACION_NUM, Integer.parseInt(session.getNumberEmp()));
        extra.putString(ACT_AFILIACION_NOM,   session.getUserFullName());
        extra.putString(ACT_AFILIACION_CEN,   session.getCentro());
        extra.putString("ipOffline",   ipOffline);
        extra.putString("url",   ipOffline + "/ligasexternas/index.html?empleado="+Integer.parseInt(session.getNumberEmp()));
        intent.putExtras(extra);

        startActivityForResult(intent, LLAMAR_MENU);
    }

    private void checkIntentAndStart(Intent intent, int idActivity, String application){
        PackageManager manager = this.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            startActivityForResult(intent, idActivity);
        } else {
            MessageDialog aviso = new MessageDialog();
            aviso.setTitle("Falta de aplicación");
            aviso.setDefaultAceptar("Es necesario instalar la aplicación: <b>" + application + "<b>");
            showMessage(this, aviso);
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
