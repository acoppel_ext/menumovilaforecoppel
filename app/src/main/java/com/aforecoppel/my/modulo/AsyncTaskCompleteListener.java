package com.aforecoppel.my.modulo;

public interface AsyncTaskCompleteListener<T> {

    void onTaskComplete(T result, int id);

}

