package com.aforecoppel.my.modulo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SesionModuloManager {

    private Context _context;
    private SharedPreferences pref; // Shared Preferences
    private Editor editor; // Editor for Shared preferences

    //Clase que se utiliza para iniciar sesión, necesaria para las redirecciones
    private Class loginClass = LoginActivity.class;

    private static final String PREF_NAME = "PREF_MODULO_LOGIN"; // Nombre del archivo de pref
    private static final String IS_LOGIN = "IS_LOGIN"; // Key para estatus de sesión

    //PARA SESIÓN DEL MENÚ DE MODULO
    private static final String PREF_FECHA       = "FECHA_SESION";   //Fecha de la sesión
    private static final String PREF_NUMEMP      = "NUMERO_PROM";    //Número de empleado del promotor
    private static final String PREF_NOMBRE      = "NOMBRE_PROM";    //Nombre de promotor
    private static final String PREF_APATER      = "APATER_PROM";    //Apellido paterno
    private static final String PREF_AMATER      = "AMATER_PROM";    //Apellido matern
    private static final String PREF_NOMCEN      = "NOMBRE_CENTRO";  //Nombre del centro
    private static final String PREF_CLACONS     = "CLAVE_CONSAR";   //Clave Consar
    private static final String PREF_CURP        = "CURP_PROM";      //Curp promotor
    private static final String PREF_CONTRA      = "CONTRA_PROM";    //Contraseña el promotor
    private static final String PREF_PASSSAFRE   = "PASS_SAFRE";     //Contraseña Safre
    private static final String PREF_NUMTIENDA   = "NUM_TIENDA";     //Numero tienda
    private static final String PREF_NUMCEN      = "NUM_CENTRO";     //Numero de centro

    @SuppressLint("CommitPrefEdits")
    SesionModuloManager(Context context){
        int PRIVATE_MODE = 0;

        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Crear la sesión
     * */
    public void createLoginSession(String fechasesion, String empleado, String nombre, String appaterno, String apmaterno,
                                   String nomCentro, String claveCon, String curpProm, String contraProm,
                                   String passSaf, String numTienda, String numCentro){

        editor.putBoolean(IS_LOGIN, true); // Storing login value as TRUE
        editor.putString(PREF_FECHA,    fechasesion); //Fecha
        editor.putString(PREF_NUMEMP,   empleado); // Storing name in pref
        editor.putString(PREF_NOMBRE,   nombre);
        editor.putString(PREF_APATER,   appaterno);
        editor.putString(PREF_AMATER,   apmaterno);
        editor.putString(PREF_NOMCEN,   nomCentro);
        editor.putString(PREF_CLACONS,  claveCon);
        editor.putString(PREF_CURP,     curpProm);
        editor.putString(PREF_CONTRA,   contraProm);
        editor.putString(PREF_PASSSAFRE,passSaf);
        editor.putString(PREF_NUMTIENDA,numTienda);
        editor.putString(PREF_NUMCEN,   numCentro);

        editor.commit(); // commit changes
    }

    /**
     * El método CheckLogin revisará el estatus del usuario
     * Si es falso, lo va a redirigir a la pantalla de login
     * En caso contrario no hará nada
     * */
    public void checkLogin(){

        // Check login status
        if(!this.isLoggedIn()){
            // El usuario no se encuentra loggeado, así que se manda al LoginActivity
            Intent i = new Intent(_context, loginClass);
            // Se cierran todas las actividades
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Se agrega una nueva Flag para comenzar una nueva Actividad
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // iniciar actividad de login
            _context.startActivity(i);
        }

    }

    /**
     * Cierra sesión y limpia los datos
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // Después de cerrar sesión se redirige al usuario al LoginActivity
        Intent i = new Intent(_context, loginClass);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        _context.startActivity(i);
    }

    /**
     * Revisar si existe una sesión
     * **/
    private boolean isLoggedIn(){

        String fecha;
        Boolean log = false;

        if(pref.getBoolean(IS_LOGIN, false)){
            fecha = pref.getString(PREF_FECHA, "");

            Date sysDate = Calendar.getInstance().getTime();
            DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String todayTime = timeFormat.format(sysDate);

            if(fecha.equals(todayTime))
                log = true;
            else{
                editor.clear();
                editor.commit();
            }
        }

        return log;
    }

    /**
     * Metodos para obtener información
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();
        // Número de empleado
        user.put(PREF_NUMEMP,   pref.getString(PREF_NUMEMP, null));
        user.put(PREF_NOMBRE,   pref.getString(PREF_NOMBRE, null));
        user.put(PREF_APATER,   pref.getString(PREF_APATER, null));
        user.put(PREF_AMATER,   pref.getString(PREF_AMATER, null));
        user.put(PREF_NOMCEN,   pref.getString(PREF_NOMCEN, null));
        user.put(PREF_CLACONS,  pref.getString(PREF_CLACONS, null));
        user.put(PREF_CURP,     pref.getString(PREF_CURP, null));
        user.put(PREF_CONTRA,   pref.getString(PREF_CONTRA, null));
        user.put(PREF_PASSSAFRE, pref.getString(PREF_PASSSAFRE, null));
        user.put(PREF_NUMTIENDA, pref.getString(PREF_NUMTIENDA, null));
        user.put(PREF_NUMCEN,   pref.getString(PREF_NUMCEN, null));

        // return user
        return user;
    }

    public String getNumberEmp(){
        return pref.getString(PREF_NUMEMP, null);
    }

    public String getUserFullName(){
        return pref.getString(PREF_NOMBRE, null) + " " + pref.getString(PREF_APATER, null) +
                " " + pref.getString(PREF_AMATER, null);
    }

    public String getCurp(){
        return pref.getString(PREF_CURP, null);
    }

    public String getCentro(){
        return pref.getString(PREF_NUMCEN, null) + " " + pref.getString(PREF_NOMCEN, null);
    }


    public String getNomCentro(){
        return pref.getString(PREF_NOMCEN, null)+"";
    }

    public String getNumCentro(){
        return pref.getString(PREF_NUMCEN, null)+"";
    }

    public String getClaConsar(){
        return pref.getString(PREF_CLACONS, "0")+"";
    }
}