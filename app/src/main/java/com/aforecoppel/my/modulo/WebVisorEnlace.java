package com.aforecoppel.my.modulo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import static com.aforecoppel.my.modulo.Utilerias.ACT_AFILIACION_NUM;
import static com.aforecoppel.my.modulo.Utilerias.CONFIG_KEY1;
import static com.aforecoppel.my.modulo.Utilerias.LLAMAR_WEB_ENLACE;
import static com.aforecoppel.my.modulo.Utilerias.writeLog;

public class WebVisorEnlace extends AppCompatActivity {

    private String ipAddress;
    private int iEmpleado;
    private String sUrl;
    private WebView mWebViewEnlace;

    //VARIABLES DE LA ACTIVIDAD
    private RequestQueue queue;
    private int tipoFirmaD;
    private String opcionFirmas;
    public static Context context;
    private String nameFirma = "";
    private String sFolioIne = "0";
    private String ipOffline = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_enlace);
        Intent intent = getIntent();
        getParameters(intent);
        context = this;
        queue = Volley.newRequestQueue(this);

        mWebViewEnlace = (WebView) findViewById(R.id.webviewenlace);
        WebSettings mWebSettings = mWebViewEnlace.getSettings();
        mWebViewEnlace.setWebViewClient(new WebViewClient());
        mWebSettings.setJavaScriptEnabled(true);
        JavaScriptInterface jsInterface = new JavaScriptInterface();
        mWebViewEnlace.addJavascriptInterface(jsInterface, "Android");
        mWebViewEnlace.loadUrl(sUrl);
    }

    public void getParameters(Intent intent){

        ipAddress = intent.getStringExtra(CONFIG_KEY1);
        ipOffline = intent.getStringExtra("ipOffline");
        iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
        sUrl = intent.getStringExtra("url");

    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void llamaComponenteEnlace(String url) {
            writeLog( "llamaComponenteEnlace url " + url);

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_ENLACE);

        }
    }

}
